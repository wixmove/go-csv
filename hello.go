package main

import (
	"encoding/csv"
	"fmt"
	"os"
)
type JobsList struct {
    profession_id string
    contract_type string
}

type JobsCategorie struct {
    id string
    category_name string
}

func main() {

	JobsCategorieList, err := readCsvData("data/technical-test-professions.csv")
	JobsDataList, err := readCsvData("data/technical-test-jobs.csv")

    if err != nil {
        panic(err)
    }
	for _, category := range JobsCategorieList {
        data := JobsCategorie{
            id: category[0],
            category_name: category[2],
		}
		fmt.Println(data.id + " " + data.category_name)
    }
    
    for _, job := range JobsDataList {
        data := JobsList{
            profession_id: job[0],
            contract_type: job[1],
		}
		fmt.Println(data.profession_id + " " + data.contract_type)
		
    }
}

func readCsvData(filename string) ([][]string, error) {
    // Ouverture du fichier CSV
    f, err := os.Open(filename)
    if err != nil {
        return [][]string{}, err
    }
    defer f.Close()

    // Ecriture du fichier dans une variable
    lines, err := csv.NewReader(f).ReadAll()
    if err != nil {
        return [][]string{}, err
    }

    return lines, nil
}